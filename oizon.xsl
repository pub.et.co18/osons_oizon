<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0">
    <xsl:output method="xhtml"
        doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"/>
    <xsl:template match="TEI">
        <html>
            <head>
                <link href="cesaire_dorleans.css" type="text/css" rel="stylesheet"/>
                <meta http-equiv="Content-type" content="text/html; charset=UTF8"/>
                <title>
                    <xsl:value-of select="normalize-space(teiHeader/fileDesc/titleStmt/title)"/>
                </title>
            </head>
            <body>
                        
            </body>
        </html>
        <xsl:result-document href="oizon_ead.xml"
            doctype-public="+//ISBN 1-931666-00-8//DTD ead.dtd (Encoded Archival Description (EAD) Version 2002)//EN"
            doctype-system="ead.dtd">
            <ead>
                <eadheader>
                    <eadid>
                        osons_oizon
                    </eadid>
                    <filedesc>
                        <titlestmt>
                            <titleproper>
                                Projet Osons Oizon
                                
                            </titleproper>
                            <author>
                                Projet Osons Oizon
 
                            </author> 
                        </titlestmt>
                        <publicationstmt>
                            <publisher>
                                Archives Départementales du Cher
                                <date><xsl:variable name="nameOfMyDate" select="current-date()"/><xsl:value-of select="$nameOfMyDate"/></date>
                            </publisher>
                        </publicationstmt>
                    </filedesc>
                    <profileDesc>
                        <creation>Ce fichier a été générés à partir de fichier XML TEI dont les informations ont été moisonnées grâce à XSLT</creation>
                        <langusage>Instrument de recherche rédigé en <language langcode="fre">français</language></langusage>
                    </profileDesc>
                </eadheader>
                <archdesc level="item">
                    <did>
                        <unitid identifier="OSONS/OIZON">OSONS OIZON</unitid>
                        <unittitle>Fichiers EAD du projet Osons Oizon</unittitle>
                        <unitdate normal="1714-01-01/1921-12-31" type="inclusive">1714-1921</unitdate>
                        <physdesc>
                            <extent unit="article(s)"></extent>
                        </physdesc>
                    </did>
                    <accessrestrict type="formate">
                        <p>Communicable</p>
                    </accessrestrict>
                    <acqinfo>
                        <p>Type d'entrée : depot</p>
                        <p>Service Versant : Commune de Oizon</p>
                    </acqinfo>
                    <arrangement>
                        <p>État de la cotation : Définitive</p>
                    </arrangement>
                    <scopecontent>
                        <p>Archives communales de Oizon</p>
                    </scopecontent>
                    <dsc type="in-depth">
                        <c id="a0114666763506lcn9s" level="file">
                            <did>
                                <unitid identifier="EDEPOT/1690">EDEPOT/1690</unitid>
                                <unittitle>Registres paroissiaux ou d'état-civil (lacunes pour 1748)</unittitle>
                                <unitdate normal="1739-01-01/1755-12-31" type="inclusive">1739-1755</unitdate>
                                <physdesc>
                                    <genreform>Papier</genreform>
                                </physdesc>
                                <langmaterial>Français</langmaterial>
                                
                            </did>
                        </c>
                    </dsc>
                    
                    
                    <xsl:for-each select="//msItem">
                        
                        <dsc>
                            <c>
                                <did>
                                    <unittitle>
                                        <xsl:apply-templates select="//msItem/title"> </xsl:apply-templates>
                                    </unittitle>
                                    <repository>
                                        <xsl:apply-templates select="//repository"/>
                                    </repository>
                                    <origination>
                                        <xsl:apply-templates select="//history"/>
                                    </origination>
                                    <physdesc>
                                        
                                        <dimensions>
                                            <xsl:value-of select="//dimensions/height"/> ×
                                            <xsl:value-of select="//dimensions/width"/>
                                        </dimensions>
                                        <extent>
                                            <xsl:value-of select="//dim"/> pages
                                        </extent>
                                        <physfacet type="support">
                                            <xsl:apply-templates select="//supportDesc/support">
                                            </xsl:apply-templates>
                                        </physfacet>
                                        <physfacet type="binding">
                                            <xsl:apply-templates select="//bindingDesc"/>
                                        </physfacet>
                                        <physfacet type="hand">  
                                            <xsl:apply-templates select="//handDesc"/>
                                        </physfacet>
                                        <physfacet type="foliation">
                                            <xsl:apply-templates select="//foliation"/>
                                        </physfacet>
                                        
                                        
                                    </physdesc>
                                    <unitdate>
                                        <xsl:apply-templates select="//docDate"/>
                                    </unitdate>
                                </did>
                            </c>
                        </dsc>
                    </xsl:for-each>
                </archdesc>
            </ead>
        </xsl:result-document>
    </xsl:template>
    
    <xsl:template match="teiHeader"/>
    
    <xsl:template match="div">
        <section>
            <h3>
                <xsl:number format="I. " level="any"/>
                <xsl:value-of select="@type"/>
            </h3>
            <xsl:apply-templates/>
        </section>
    </xsl:template>
    
    
    <xsl:template match="metamark"/>
    <xsl:template match="del">
        <del>
            <xsl:apply-templates/>
        </del>
    </xsl:template>
    <xsl:template match="lb">
        <br/>
    </xsl:template>
    <xsl:template match="add">
        <sup>
            <xsl:apply-templates/>
        </sup>
    </xsl:template>
    <xsl:template match="pb">
        <p>
            <xsl:number format="1. " level="any"/>
            <a href="{@facs}">Click to open image of page <xsl:value-of select="@n"/></a>
        </p>
    </xsl:template>
    
    <xsl:template match="*[@rend = 'indent']">
        <p style="text-indent:25px">
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="*[@rend = 'right']">
        <p align="right">
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="*[@rend = 'left']">
        <p align="left">
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    
    <xsl:template match="*[@rend = 'center']">
        <p style="text-align:center">
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    
    
</xsl:stylesheet>
